This is using some really simple simulations to see how some very simplified
index swapping simulations may be normalized. The aim is to make amplicon-seq
approaches to COVID19 testing work better for actual certification !!!

LG/TTC said something about treating small problems as if they're hard, and
thus finding nothing difficult.

Anyways, plan:

- setup a simulation where sample barcodes are a number, so a pair of barcodes 
    is 1_1 or 51_34. Give each sample an abundance of real counts based on a 
    distribution (likely a bimodal mixture of lognormal), 
    then a noise distribution (poisson, gamma?).
- pull this into a graph, so likely networkx, and then apply a normalization
- compare this to baseline distribution, calculate some noise metrics and 
    summary plots
- test this across a variety of sample parameters of noise and such
