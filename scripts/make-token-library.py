#!/usr/bin/python3

import argparse
import numpy as np
#import yaml
#import re
import itertools
import random
import copy
import pandas as pd
import sys

# cuz I dunno
if __name__ == '__main__':

    # Command line arguments. Can you tell I like to argue with my scripts?
    # More flexible for pipelines, I can make parameter combos with nextflow
    # and pass them in with verbose flags!

    parser = argparse.ArgumentParser(description=\
            "This script makes a toy token model for thinking about how to "+
            "deal with index barcode swapping."
        )

    # Dummy run ID name
    parser.add_argument("--run-id",default="", type=str,
        help="Dummy run ID name")

    # How many of each index are we combining? i5 and i7 here
    parser.add_argument("--number-of-i5",default=16, type=int,
        help="The number of different i5 indicies.")
    parser.add_argument("--number-of-i7",default=24, type=int,
        help="The number of different i7 indicies.")

    # How many samples are positives (have target in there) ?
    parser.add_argument("--fraction-samples-positive",default=0.5, type=float,
        help="Fraction of samples (assigned indicies) that are positives.")

    # How many targets and controls do we have? Usually going to be fixed to
    # one control and two targets, as that's the current assay.
    parser.add_argument("--number-of-controls",default=1, type=int,
        help="The number of different controls to amplify.")
    parser.add_argument("--number-of-targets",default=2, type=int,
        help="The number of different targets (not controls) to amplify.")

    # How much has gotten amplified during the PCR? This is contemplating how
    # many fragments of DNA (not to scale) are in the well after each RT-PCR
    # reaction, so we'll sample this many fragments from the mix of target
    # and control. I'm thinking it as being ng DNA?
    parser.add_argument("--amount-per-sample",default=1e2, type=float,
        help="")
    parser.add_argument("--sample-amount-model",default="normal", type=str,
        help="")
    parser.add_argument("--sample-amount-dispersion",default=1e1, type=float,
        help="")
    parser.add_argument("--sample-amount-minimum",default=1e1, type=float,
        help="")

    # What's the ratio of the target (viral here) to controls? This is the
    # actual model of how does the COVID19 viral load differ between samples,
    # for now a normal.
    parser.add_argument("--ratio-of-target-to-control",default=1, type=float,
        help="")
    parser.add_argument("--ratio-variation-model",default="normal", type=str,
        help="")
    parser.add_argument("--ratio-variation-dispersion",default=0.5, type=float,
        help="")
    parser.add_argument("--ratio-minimum",default=0.01, type=float,
        help="")

    # How many counts are we sampling out of each well?
    parser.add_argument("--total-counts",default=384e3, type=float,
        help="")
    parser.add_argument("--counts-variation-model",default="poisson", type=str,
        help="")
    parser.add_argument("--counts-variation-dispersion",default=None, type=float,
        help="")

    # Actual (not observed) rate of swapping?
    parser.add_argument("--index-swap-rate",default=0.05, type=float,
        help="")

    # Files to write out to?
    parser.add_argument("output",default="out", type=str,
        help="The filename to write, fasta")

    args = parser.parse_args()

    #
    #
    # WE BEGIN
    #
    #

    baseNameOut = args.output
    print("Printing this out to: "+baseNameOut)

    # All indicies
    i5z = [ "i5#"+i for i in map(chr,range(97,97+args.number_of_i5)) ]
    i7z = [ "i7#"+i for i in map(chr,range(97,97+args.number_of_i7)) ]

    datar = pd.DataFrame(
            itertools.product(i5z,i7z), # Making pandaframe of all index combos
            columns=["index5","index7"]
            ) \
        .assign(
            true_positive=lambda x: # This is if the sample is a true positive
                np.random.uniform(size=x.shape[0]) <= # ie COVID19
                    args.fraction_samples_positive
            ,
            sample_amount=lambda x: # This determines how much of that sample
                np.clip(            # is mixed into the sequencing reaction,
                    np.random.normal( # post PCR and all that.
                        loc=args.amount_per_sample,
                        scale=args.sample_amount_dispersion,
                        size=x.shape[0]
                        ) ,
                    a_min=args.sample_amount_minimum, a_max=None
                    )
            ) \
        .assign(sample_fraction=lambda x: # This is how much of the total that
            x.sample_amount / np.sum(x.sample_amount) # particular sample is
            ) \
        .assign(this_ratio=lambda x: # Next, we calculate a random ratio of the
            x.true_positive * np.clip( # target amplicons (viral) to the 
                np.random.normal( # standard (synthetic or human or whatever)
                    loc=args.ratio_of_target_to_control,
                    scale=args.ratio_variation_dispersion,
                    size=x.shape[0]
                    ),
                a_min=args.ratio_minimum, a_max=None
                )
            ) \
        .assign(
            control_counts=lambda x:
                [   {"control_"+str(i): 
                        np.clip( 
                            np.random.poisson(
                                sample_frac*args.total_counts /
                                    ( args.number_of_controls + 
                                        ratio*args.number_of_targets ) 
                                ,
                                size=args.number_of_controls
                                ),
                            a_min=0, a_max=None
                            )[0] for i in range(args.number_of_controls)
                        } for sample_frac,ratio in 
                            zip(x.sample_fraction,x.this_ratio)
                    ]
            ,
            target_counts=lambda x:
                [   {"target_"+str(i): 
                        np.clip( 
                            np.random.poisson(
                                ratio*sample_frac*args.total_counts /
                                    ( args.number_of_controls + 
                                        ratio*args.number_of_targets ) 
                                ,
                                size=args.number_of_controls
                                ),
                            a_min=0, a_max=None
                            )[0] for i in range(args.number_of_targets)
                        } for sample_frac,ratio in 
                            zip(x.sample_fraction,x.this_ratio)
                    ]
            ) \
        .melt(
            id_vars=["index5","index7","true_positive","sample_amount",
                "sample_fraction","this_ratio"],
            value_vars=["control_counts","target_counts"]
            ) \
        .sort_values(by=['variable','true_positive']) \
        .set_index(['index5','index7']) \
        .assign(target=lambda x: [ sorted(i.keys()) for i in x.value ]) \
        .assign(valuez=lambda x: [ [ i[j] for j in sorted(i.keys()) ] for i in x.value ]) \
        .assign(valz=lambda x: [ [ (k,l) for k,l in zip(i,j) ] for i,j in zip(x.target,x.valuez) ] ) \
        .drop(columns=["sample_amount","sample_fraction",
            "this_ratio",'variable','value','valuez','target']) \
        .explode('valz') \
        .assign(target=lambda x: [ i for i,j in x.valz] ) \
        .assign(counts=lambda x: [ j for i,j in x.valz] ) \
        .drop(columns=["valz"]) \
        .sort_values(by=['index5','index7','true_positive','target'])


    print(datar)
    print("")

    print("generated baseline, simulating swapping...")

    def swaperoo(this_datar):
    
        print(this_datar)

        if np.sum(this_datar.counts) == 0:
            print("ZERO COUNTS, probs bad data sim")
            swap_noise = { i5: { i7: 0 for i7 in i7z } for i5 in i5z }
            swap_this_datar = this_datar \
                .set_index(keys=['index5','index7']) \
                .join( \
                    pd.DataFrame(swap_noise) \
                        .assign(index7=lambda x: x.index) \
                        .melt(id_vars=['index7'],var_name='index5',
                            value_name='swap_noise') \
                        .set_index(keys=['index5','index7'])
                    ,
                    on=['index5','index7']
                    )
            return swap_this_datar

        rows_to_swap = \
            np.random.choice(
                this_datar.index,
                p=this_datar.counts/np.sum(this_datar.counts),
                size=int(args.index_swap_rate*np.sum(this_datar.counts))
                )
    
        rows_to_swap_with = \
            np.random.choice(
                this_datar.index,
                p=this_datar.counts/np.sum(this_datar.counts),
                size=int(args.index_swap_rate*np.sum(this_datar.counts))
                )

        # Gotta swap each one individually, and symetrically
        swap_noise = { i5: { i7: 0 for i7 in i7z } for i5 in i5z }
    
        for i,j in zip(rows_to_swap,rows_to_swap_with):
            ( si5, si7, stp, svar, sval ) = this_datar.iloc[i,]
            ( ti5, ti7, ttp, tvar, tval ) = this_datar.iloc[j,]
            swap_noise[si5][si7] -= 1
            swap_noise[ti5][ti7] -= 1
            swap_noise[si5][ti7] += 1
            swap_noise[ti5][si7] += 1
    
        swap_this_datar = this_datar \
            .set_index(keys=['index5','index7']) \
            .join( \
                pd.DataFrame(swap_noise) \
                    .assign(index7=lambda x: x.index) \
                    .melt(id_vars=['index7'],var_name='index5',
                        value_name='swap_noise') \
                    .set_index(keys=['index5','index7'])
                ,
                on=['index5','index7']
                )
    
        return swap_this_datar
    
    swapped_datar = datar.groupby(by='target') \
        .apply(lambda x: swaperoo(x.reset_index()))

    swapped_datar.to_csv(args.output)

