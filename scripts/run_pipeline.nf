// 
//
//

// Put your email here for notifications of when it's done, useful for remote
email_address = 'darachm@stanford.edu'

// Making some directories to hold outputs
file("./tmp").mkdirs()      // temporary files, really useful for debugging
file("./reports").mkdirs()  // nextflow's internal reports, performance etc
file("./output").mkdirs()   // nice polished outputs



//
//
// parmz
//
//

// Setting up parameters for generation, doing this way so that it's very 
//   flexible and generation/analysis scales nicely.

simulation_parameters = Channel.from( [   

        [   "run-id": ['a','b','c'],
            "number-of-controls": ['1'],
            "number-of-targets": ['1'],
            "number-of-i5": ['4'],
            "number-of-i7": ['4'],
            "fraction-samples-positive": ['0.05'],
            "ratio-of-target-to-control": ['1.0'],
            "ratio-variation-dispersion": ['0.2'],
            "total-counts": ['16e3'],
            "index-swap-rate": ['0.05'],
            ],

        [   "run-id": ['a','b','c'],
            "number-of-controls": ['1'],
            "number-of-targets": ['1'],
            "number-of-i5": ['16'],
            "number-of-i7": ['24'],
            "fraction-samples-positive": ['0.05'],
            "ratio-of-target-to-control": ['1.0'],
            "ratio-variation-dispersion": ['0.2'],
            "total-counts": ['384e3'],
            "index-swap-rate": ['0.05'],
            ],

        ]

        .collect{ 
            it.collect{ k, v -> v.collect{ [(k):it] }}
                .combinations().collect{it.collectEntries()} 
            }
        .flatten()
    )
//simulation_parameters.subscribe{println it}

//    .concat(Channel.from(
//        [   bname:["six-base-biased","eight-base-biased","ten-base-biased","twelve-base-biased"],
//            lineages:[16,32,64,96],
//            codesper:[1],fixedper:["True"],librep:[1,2,3],
//            graph_args:[" --use-lev-dist --graph-max-dist 6 "],
//        ].collect{ k, v -> v.collect{ [(k):it] } }.combinations().collect{ it.collectEntries() }
//        )   )


parameters_making_libraries = simulation_parameters
    .map{ it.collect{ k,v -> "--${k} ${v}" }.join(" ") }
//parameters_making_libraries.subscribe{println it}
//
////    .cross(library_parameters)
////    .map{ it[0] + it[1] } 
////    .concat(Channel.from(PoisonPill.instance))

//
//
// Processes to make these
//
//

// This is a helper function to sanitize parameters back to a good name for a
//   file.
make_safe_name = { 
//    it.each{ key, value -> key+'='+value }
//        .collect().join('_')
    it
        .replaceAll("[\\(\\)<>]","")
        .replaceAll(",",".") 
        .replaceAll(/-([a-zA-Z])[a-zA-Z]+/){ itz -> itz[1..-1].each{a -> "-"+a+"-"}.join("") }
        .replaceFirst("-","") 
        .replaceAll(" ","")
    } 

//parameters_making_libraries.subscribe{ println make_safe_name(it) }

mtl_py = Channel.fromPath("scripts/make-token-library.py")
process make_barcoded_libraries {
    input: 
        each file(script) from mtl_py
        val parameter_string from parameters_making_libraries
    output: 
        file("*.csv") into simz
    shell:
    '''
    python3 !{script} !{make_safe_name(parameter_string)}.csv !{parameter_string}
    '''
}

acsv_r = Channel.fromPath("scripts/analyze_csv.R")
process analyze_simulated_csvs {
    input: 
        each file(script) from acsv_r
        file("*") from simz.collect()
    output:
        file("*") into analysis_outputs
    shell:
    '''
    Rscript -e "knitr::spin('!{script}')"
    '''
}


// Special trigger for `onComplete`. I copied this from documentation.
// Some predefined variables. It somehow mails it. Cool.
workflow.onComplete {
    println "Pipeline completed at: $workflow.complete"
    println "Execution status: ${ workflow.success ? 'OK' : 'failed' }"
    def subject = 'barnone run'
    def recipient = email_address
//    ['mail', '-s', subject, recipient].execute() << """
//
//    Pipeline execution summary
//    ---------------------------
//    Completed at: ${workflow.complete}
//    Duration        : ${workflow.duration}
//    Success         : ${workflow.success}
//    workDir         : ${workflow.workDir}
//    exit status : ${workflow.exitStatus}
//    Error report: ${workflow.errorReport ?: '-'}
//    """
}

